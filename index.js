const querystring = require('querystring')
const https = require('https')
const http = require('http')
const Stream = require('stream').Transform
const zlib = require('zlib')

const MULTIPART_BOUNDARY = 'letmein'

/*
interface RequestOptions {
  headers?: any,
  cookies?: any,
  body?: any | String,
  contentType?: String,
  method?: 'POST' | 'GET' | 'DELETE' | 'PUT'
}

interface ResponseOptions {
  encoding?: String | 'utf8'
}

interface WebResponse extends IncomingMessage {
  body?: any | String,
  cookies?: any
}
*/

const CONTENT_TYPES = {
  JSON: 'application/json',
  FORM_URL_ENCODED: 'application/x-www-form-urlencoded',
  MULTIPART_FORM_DATA: `multipart/form-data; boundary=${MULTIPART_BOUNDARY}`
}

const USER_AGENTS = {
  Win10Edge: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.246',
  ChromeOS: 'Mozilla/5.0 (X11; CrOS x86_64 8172.45.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.64 Safari/537.36',
  MacOSX: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/601.3.9 (KHTML, like Gecko) Version/9.0.2 Safari/601.3.9',
  Win7Chrome: 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36',
  LinuxFirefox: 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:15.0) Gecko/20100101 Firefox/15.0.1',
  GalaxyS9: 'Mozilla/5.0 (Linux; Android 8.0.0; SM-G960F Build/R16NW) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.84 Mobile Safari/537.36',
  iPhoneX: 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1',
  Lumia950: 'Mozilla/5.0 (Windows Phone 10.0; Android 4.2.1; Microsoft; Lumia 950) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Mobile Safari/537.36 Edge/13.1058',
  NvidiaShield: 'Mozilla/5.0 (Linux; Android 6.0.1; SHIELD Tablet K1 Build/MRA58K; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/55.0.2883.91 Safari/537.36'
}

module.exports = {
  get,
  post,
  CONTENT_TYPES,
  USER_AGENTS
}

/**
 * @param {String} url
 * @param {{headers?, cookies?, body?, contentType?, method?}} requestOptions
 * @param {{encoding}} responseOptions
 */
async function get (url, requestOptions = {}, responseOptions = {}) {
  requestOptions.method = 'GET'
  if (!url) {
    throw new Error('url must not be empty')
  }
  return makeRequest(url, requestOptions, responseOptions)
}

/**
 * @param {String} url
 * @param {{headers?, cookies?, body?, contentType?, method?}} requestOptions
 * @param {{encoding}} responseOptions
 */
async function post (url, requestOptions = {}, responseOptions = {}) {
  if (!url) {
    throw new Error('url must not be empty')
  }
  if (!requestOptions.headers) { requestOptions.headers = {} }
  const headers = requestOptions.headers || {}
  const body = requestOptions.body

  requestOptions.method = 'POST'

  if (!headers['Content-Type']) {
    headers['Content-Type'] = requestOptions.contentType || CONTENT_TYPES.FORM_URL_ENCODED
  }

  if (typeof (body) === 'object') {
    if (headers['Content-Type'].startsWith(CONTENT_TYPES.JSON)) {
      requestOptions.body = JSON.stringify(body)
    } else if (headers['Content-Type'].startsWith(CONTENT_TYPES.FORM_URL_ENCODED)) {
      requestOptions.body = querystring.stringify(body)
    } else if (headers['Content-Type'].startsWith(CONTENT_TYPES.MULTIPART_FORM_DATA)) {
      requestOptions.body = makeMultipartBody(body)
    }
    requestOptions.headers['Content-Length'] = requestOptions.body.length
  }
  return makeRequest(url, requestOptions, responseOptions)
}

function makeMultipartBody (bodyObj) {
  let text = `--${MULTIPART_BOUNDARY}`
  const keys = Object.keys(bodyObj)
  for (let i = 0; i < keys.length; i++) {
    const key = keys[i]
    text += `\r\nContent-Disposition: form-data; name="${key}"\r\n\r\n${bodyObj[key]}\r\n--${MULTIPART_BOUNDARY}`
  }
  text += '--'
  return text
}

/**
 * @param {String} url
 * @param {{headers?, cookies?, body?, contentType?, method?}} requestOptions
 * @param {{encoding}} responseOptions
 */
function makeRequest (url, requestOptions, responseOptions) {
  if (!requestOptions.headers) { requestOptions.headers = {} }
  const { headers, body, method, cookies } = requestOptions
  const { encoding } = responseOptions
  if (cookies) {
    headers.Cookie = querystring.stringify(cookies, '; ')
  }
  if (!headers['User-Agent']) {
    headers['User-Agent'] = USER_AGENTS.Win10Edge
  }
  return new Promise((resolve, reject) => {
    const controller = url.startsWith('https') ? https : http
    const req = controller.request(url, { headers, method }, (res) => {
      let data = []
      const dataStream = new Stream()
      if (encoding) {
        res.setEncoding(encoding)
      }

      if (res.headers['content-encoding'] === 'gzip') {
        const gunzip = zlib.createGunzip()
        res.pipe(gunzip)
        gunzip.on('data', chunk => {
          data.push(chunk)
        })
        gunzip.on('end', onEnd)
      } else {
        res.on('data', (chunk) => {
          if (res.headers['content-type'].startsWith('image/png')) {
            dataStream.push(chunk)
          } else {
            data.push(chunk)
          }
        })
        res.on('end', onEnd)
      }
      function onEnd () {
        if (res.headers['content-encoding'] === 'gzip') {
          let str = ''
          for (let i = 0; i < data.length; i++) {
            const chunk = data[i]
            str += chunk.toString()
          }
          data = str
        }

        const contentType = res.headers['content-type'] || ''
        if (contentType.startsWith('application/json') || contentType.startsWith('application/x-json')) {
          const jsonStr = Array.isArray(data) ? data.join('') : data.toString()
          try {
            res.body = JSON.parse(jsonStr)
          } catch (e) {
            res.body = jsonStr
          }
        } else if (contentType.startsWith('application/octet-stream')) {
          try {
            res.body = Buffer.concat(data)
          } catch (e) {
            res.body = data
          }
        } else if (contentType.startsWith('image/png')) {
          res.body = dataStream.read()
        } else if (typeof (data) === 'string') {
          res.body = data
        } else {
          res.body = data.join('')
        }
        if (res.headers && res.headers['set-cookie']) {
          res.cookies = interpretCookies(res.headers['set-cookie'])
        }
        resolve(res)
      }
      res.on('error', err => {
        reject(err)
      })
    })

    req.on('error', (e) => {
      console.error(`problem with request: ${e.message}`)
      reject(e)
    })

    if (body) {
      req.write(body)
    }
    req.end()
  })
}

function interpretCookies (setCookieHeader) {
  const cookies = {}
  for (let i = 0; i < setCookieHeader.length; i++) {
    const cookie = setCookieHeader[i]
    const name = cookie.substring(0, cookie.indexOf('='))
    const value = cookie.substring(cookie.indexOf('=') + 1, cookie.indexOf(';'))
    cookies[name] = value
  }
  return cookies
}
